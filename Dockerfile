FROM php:7.3.28-apache

RUN apt-get update \
&& apt-get install -y \
default-mysql-server 

RUN docker-php-ext-install mysqli

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

COPY ./src/admin.php /var/www/html/
COPY ./src/verify.php /var/www/html/
COPY ./src/index.php /var/www/html/

ENV MYSQL_ROOT_PASSWORD root
ENV MYSQL_DATABASE_COVID covid_db
ENV MYSQL_DATABASE_FLAG flag_db
ENV MYSQL_USER_COVID hc_reader
ENV MYSQL_USER_FLAG hc_admin
ENV MYSQL_PASSWORD_COVID hc_reader
ENV MYSQL_PASSWORD_FLAG hc_admin 
ENV MYSQL_USER_MONITORING monitoring
ENV MYSQL_PASSWORD_MONITORING monitoring

# Installing packages MariaDB
RUN addgroup mysql mysql

RUN cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini

# Work path
WORKDIR /tmp

# Copy of the MySQL startup script
COPY scripts/set_up_sql.sh start.sh

# Creating the persistent volume
VOLUME [ "/var/lib/mysql" ]

EXPOSE 80

ENTRYPOINT [ "./start.sh" ]
