#!/bin/sh
echo "*** Start of SQLSCRIPT ***"

if [ ! -d "/run/mysqld" ]; then
	mkdir -p /run/mysqld
	chown -R mysql:mysql /run/mysqld
fi

rm -rf /var/lib/mysql/mysql

if [ -d /var/lib/mysql/mysql ]; then
	echo '[i] MySQL directory already present, skipping creation'
else
	echo "[i] MySQL data directory not found, creating initial DBs"

	chown -R mysql:mysql /var/lib/mysql


	# init database
	echo 'Initializing database'
	mysql_install_db --user=mysql --datadir=/var/lib/mysql > /dev/null
	echo 'Database initialized'

	echo 'checking if database was created:'
	cd /var/lib/mysql
	ls -al | grep mysql

	echo "[i] MySql root password: $MYSQL_ROOT_PASSWORD"

	# create temp file
	tfile=`mktemp`
	if [ ! -f "$tfile" ]; then
	    return 1
	fi

	# save sql
	echo "[i] Create temp file: $tfile"
	cat << EOF > $tfile
USE mysql;
FLUSH PRIVILEGES;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY '$MYSQL_ROOT_PASSWORD' WITH GRANT OPTION;
GRANT SELECT, SHOW VIEW, PROCESS ON *.* TO '$MYSQL_USER_MONITORING'@'%' IDENTIFIED BY '$MYSQL_PASSWORD_MONITORING' WITH GRANT OPTION;
EOF

	# Create new database
	if [ "$MYSQL_DATABASE_COVID" != "" ]; then
		echo "[i] Creating database: $MYSQL_DATABASE_COVID"
		echo "CREATE DATABASE IF NOT EXISTS \`$MYSQL_DATABASE_COVID\` CHARACTER SET utf8 COLLATE utf8_general_ci;" >> $tfile

		# set new User and Password
		if [ "$MYSQL_USER_COVID" != "" ] && [ "$MYSQL_PASSWORD_COVID" != "" ]; then
		echo "[i] Creating user: $MYSQL_USER_COVID with password $MYSQL_PASSWORD_COVID"
		echo "CREATE USER '$MYSQL_USER_COVID'@'localhost' IDENTIFIED BY '$MYSQL_PASSWORD_COVID';" >> $tfile 
		echo "GRANT SELECT ON $MYSQL_DATABASE_COVID.* TO '$MYSQL_USER_COVID'@'localhost';" >> $tfile
		echo "GRANT FILE ON *.* TO '$MYSQL_USER_COVID'@'localhost';" >> $tfile
		fi
	else
		echo "[i] ERROR: Environment Variables for covid_db are not correctly set"
	fi
	
	if [ "$MYSQL_DATABASE_FLAG" != "" ]; then 
                echo "[i] Creating database: $MYSQL_DATABASE_FLAG"
                echo "CREATE DATABASE IF NOT EXISTS \`$MYSQL_DATABASE_FLAG\` CHARACTER SET utf8 COLLATE utf8_general_ci;" >> $tfile

                # set new User and Password
                if [ "$MYSQL_USER_FLAG" != "" ] && [ "$MYSQL_PASSWORD_FLAG" != "" ]; then
                echo "[i] Creating user: $MYSQL_USER_FLAG with password $MYSQL_PASSWORD_FLAG"
		echo "CREATE USER '$MYSQL_USER_FLAG'@'localhost' IDENTIFIED BY '$MYSQL_PASSWORD_FLAG';" >> $tfile 
                echo "GRANT ALL ON *.* TO '$MYSQL_USER_FLAG'@'localhost';" >> $tfile
                fi
	
	else
		echo "[i] ERROR: Environment Variables for flag_db are not correctly set"
	fi
fi

	echo 'FLUSH PRIVILEGES;' >> $tfile

	# insert certificates table into covid_db
	echo "USE covid_db;" >> $tfile
	echo "CREATE TABLE certificates (cert_id text,valid_from date,valid_till date,is_valid tinyint(1),issuer text);" >> $tfile

	echo "INSERT INTO \`certificates\`(\`cert_id\`, \`valid_from\`, \`valid_till\`, \`is_valid\`, \`issuer\`) VALUES ('HC-20211337','2021-07-01','2025-08-01','1','HC_Hospital'),
('HC-20211338','2021-01-31','2021-02-01','0','HC_Hospital'),
('HC-20211339','2021-02-21','2021-03-01','0','HC_Hospital'),
('HC-20211329','2021-01-01','2021-01-11','0','HC_Hospital'),
('HC-20211319','2021-08-01','2021-08-11','1','HC_Hospital');" >> $tfile	 

	#insert table grap_me into flag_db 
	echo "USE flag_db;" >> $tfile 
	echo 'CREATE TABLE flag (grab_me text);' >> $tfile

	echo 'CREATE TABLE users (username text,password text,allowed_ip text);' >> $tfile

	echo "INSERT INTO \`flag\` (\`grab_me\`) VALUES ('HC2021{DSgYeNRRiynip4vAPbe04jWTVrf2bBFw}');" >> $tfile

	echo "INSERT INTO \`users\` (\`username\`, \`password\`, \`allowed_ip\`) VALUES ('admin', 'f38ee51a81d14babd0fe2741b28f89fe', '127.0.0.1');" >> $tfile

	# run sql in tempfile
	echo "[i] run tempfile: $tfile"
	/usr/sbin/mysqld --user=mysql --bootstrap --verbose=4 --datadir=/var/lib/mysql <  $tfile
	rm -f $tfile

	# setting file permissions
	chown root:root /var/www/html
	chmod 644 /var/www/html/*


echo "[i] Sleeping 5 sec"
sleep 5

echo "Starting all process"
apachectl start
exec /usr/sbin/mysqld --user=mysql --console --datadir=/var/lib/mysql --verbose=3

